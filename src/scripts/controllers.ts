const getNextLng = (lng: string) => {
  const languages = ["en", "es", "pt"];
  const index = languages.findIndex((l) => l === lng);

  return languages[index === languages.length - 1 ? 0 : index + 1];
};

class Nav{
  openMenu(event){
    console.log(event)
  }
}

app.controller("AppCtrl", function ($scope, $i18next) {
  "use strict";
  $scope.currentLng = $i18next.options.lng;

  $scope.switchLng = function () {
    $i18next.changeLanguage(getNextLng($scope.currentLng));
    $scope.currentLng = $i18next.options.lng;
  };
  $scope.nav = new Nav();
});
