"use strict";
/// <reference path="../interfaces/i18next-http-backend.d.ts" />
/// <reference path="../interfaces/i18next.d.ts" />
const i18nextOptions = {
    lng: "es",
    fallbackLng: ["en", "en", "pt"],
    defaultNS: "translation",
    backend: {
        loadPath: "/src/locales/{{lng}}/{{ns}}.json",
    },
};
const i18n = window.i18next;
if (i18n) {
    i18n.use(i18nextHttpBackend);
    i18n.init(i18nextOptions, (err, t) => {
        console.log("resources loaded");
    });
}
app.filter("capitalize", function () {
    return function (token) {
        return token.charAt(0).toUpperCase() + token.slice(1);
    };
});
